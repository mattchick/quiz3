﻿using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Text;
using umbraco.cms.businesslogic.member;

namespace MC.Events
{
    public class MemberEvents : ApplicationEventHandler
    {

        public delegate void MemberCreatedEventHandler(object sender, IMember member);
        public event MemberCreatedEventHandler MemberCreated;

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            MemberService.Created += MemberService_Created;
        }

        void MemberService_MemberCreated(IMember e)
        {
            if (e.ContentTypeAlias == "Facilitator")
            {

                string userEmail = e.Email;
                string userPassword = e.RawPasswordValue;
                MailMessage email = new MailMessage();
                var member = ApplicationContext.Current.Services.MemberService.GetByUsername(userEmail);
                var st = member.GetValue<string>("displayName");


                StringBuilder body = new StringBuilder();
                body.Append("<p>Dear " + member.GetValue<string>("displayName") + "<p/>");
                body.Append("<p><strong>Quality Champion Facilitator Login and Password</strong></p>");
                body.Append("<p>You have been successfully enrolled to access the ‘Quality Every Day Online Survey’. </p>");
                body.Append("<p>The Online Survey should be used by you when facilitating <strong>online sessions</strong> with your participants.</p>");
                body.Append("<p>Below is your Login and Password details.</p>");
                body.Append("<p><strong>Username:</strong> " + e.Username + "</p>");
                body.Append("<p><strong>Password:</strong> " + e.RawPasswordValue + "</p>");
                body.Append("<p>Access the Online Survey here <a href='http://qualityeveryday.com'>http://qualityeveryday.com</a></p>");
                body.Append("<p><strong>Once you have logged in you will be able to –</strong></p><ol>");
                body.Append("<li>Set up new online sessions and add participants ahead of delivery.</li>");
                body.Append("<li>Review participant responses whilst delivering a session. </li>");
                body.Append("<li>Review all participant responses after you’ve delivered a session</li></ol></p>");
                body.Append("<p>Please keep this email as a reminder of your login details.</p><p><strong>Thank You<strong></p>");
                email.IsBodyHtml = true;
                email.Subject = "Quality Champion Facilitator Login and Password";
                email.Body = body.ToString();
                email.To.Add(e.Email);
                email.Bcc.Add("matt@mchick.co.uk");
                SmtpClient client = new SmtpClient();
                client.Send(email);
            } else
            {


                string userEmail = e.Email;
                string userPassword = e.RawPasswordValue;
                MailMessage email = new MailMessage();
                var member = ApplicationContext.Current.Services.MemberService.GetByUsername(userEmail);

                var names = e.Name.Split(' ');
                string firstName = "";
                if (names.Length > 0)
                {
                    firstName = names[0];
                }
                StringBuilder body = new StringBuilder();
                body.Append("<p>Dear " + firstName + "<p/>");
                body.Append("<p><strong>Quality Every Day Login and Password</strong></p>");
                body.Append("<p>You have been invited to attend a ‘Quality Every Day’ workshop. As part of this workshop you will be required to participate in discussions which will be facilitated through an Online Survey.</ p>");
                body.Append("<p>During the workshop, your facilitator will ask you to login to this survey.</ p>");
                body.Append("<p><strong>(Please do not access the Online Survey ahead of your Workshop)</strong></p>");
                body.Append("<p>Below is your Login and Password details.</p>");
                body.Append("<p><strong>Username:</strong> " + e.Username + "</p>");
                body.Append("<p><strong>Password:</strong> " + e.RawPasswordValue + "</p>");
                body.Append("<p>Access the Online Survey here <a href='http://qualityeveryday.com'>http://qualityeveryday.com</a></p>");
                  body.Append("<p>Please keep this email as a reminder of your login details.</p><p><strong>Thank You<strong></p>");
                email.IsBodyHtml = true;
                email.Subject = "Quality Every Day Login and Password";
                email.Body = body.ToString();
                email.To.Add(e.Email);
                email.Bcc.Add("matt@mchick.co.uk");
                SmtpClient client = new SmtpClient();
                client.Send(email);

            }
        }


        private void MemberService_Created(IMemberService sender, NewEventArgs<IMember> e)
        {
            System.Threading.Tasks.Task.Run(() => UpdateMemberAfterSaveDelayed(e.Entity)).ContinueWith(t =>
                {
                    var ex = t.Exception.InnerException;

                }, TaskContinuationOptions.OnlyOnFaulted);

        }


        /// <summary>
        /// Delegate that triggers RegisterEvent_MemberSaved after 5 secs.
        /// </summary>
        /// <param name="member"></param>
        private void UpdateMemberAfterSaveDelayed(IMember member)
        {

            System.Threading.Thread.Sleep(5000);
            MemberService_MemberCreated(member);
        }
    }
}