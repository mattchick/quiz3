﻿using Quiz3.Core.Models.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace Quiz3.Core.Services
{
    public class ActiveParticipantService 
    {
        private readonly UmbracoDatabase _db;

        public ActiveParticipantService()
        {
            _db = ApplicationContext.Current.DatabaseContext.Database;
        }

        public IEnumerable<ActiveParticipant> GetAllForSession(int sessionId)
        {
            return _db.Fetch<ActiveParticipant>("WHERE SessionId = @0", sessionId);
        }
        
        public ActiveParticipant Get(int memberId, int sessionId)
        {
            return _db.FirstOrDefault<ActiveParticipant>("WHERE MemberId = @0 AND SessionId = @1",
                memberId, sessionId);
        }

        public ActiveParticipant Insert(int memberId, int sessionId)
        {
            var record = Get(memberId, sessionId);
            if (record != null)
            {
                return record;
            }

            var newRecord = new ActiveParticipant()
            {
                SessionId = sessionId,
                MemberId = memberId,
                CreatedDate = DateTime.Now
            };

            _db.Insert(newRecord);
            return newRecord;
        }

        public void Delete(int memberId, int sessionId)
        {
            var record = Get(memberId, sessionId);
            if (record != null)
            {
                _db.Delete(record);
            }
        }

        public List<string> GetSessionsForFacilitator(int facilitatorId)
        {
            return _db.Query<string>("SELECT DISTINCT Title FROM FacilitatorSession WHERE FacilitatorId = @0", facilitatorId)
                .ToList();
        }
    }
}
