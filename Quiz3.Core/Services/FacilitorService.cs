﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Security;

namespace Quiz3.Core.Services
{
    public class FacilitorService
    {
        public static List<FacilitorModel> GetAll()
        {
            var m = ApplicationContext.Current.Services.MemberService;
            var facilitors = m.GetMembersByGroup("facilitator");
            return facilitors.Select(x => new FacilitorModel()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }
    }
}
