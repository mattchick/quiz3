﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models.DataAccess;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace Quiz3.Core.Services
{
    public class FormService
    {
        private readonly UmbracoDatabase db;
        public FormService()
        {
            db = ApplicationContext.Current.DatabaseContext.Database;
        }
        public Form Get(int memberId, Guid formId)
        {
            return db.FirstOrDefault<Form>("WHERE MemberId = @0 AND FormId = @1", memberId, formId);
        }

        public Form Insert(int memberId, Guid formId, DateTime dateTimeNow)
        {
            var record = Get(memberId, formId);
            if (record != null)
            {
                return record;
            }
            
            var newRecord = new Form()
            {
                MemberId = memberId,
                FormId = formId,
                StartTime = dateTimeNow
            };
            
            db.Insert(newRecord);
            return newRecord;
        }
    }
}
