﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Logging;

namespace Quiz3.Core.Services
{
    public class EmailService
    {
        /// <summary>
        /// Send email using smtp details from web.config
        /// </summary>
        /// <param name="emailTo"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static bool Send(string emailTo, string subject, string body)
        {
            MailMessage message = new MailMessage();
            message.To.Add(emailTo);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            try
            {
                new SmtpClient().Send(message);
                return true;
            }
            catch(Exception ex)
            {
                LogHelper.Error<EmailService>("Error sending email - " + ex.Message, ex);
                return false;
            }
        }

        /// <summary>
        /// Send login details to member in a plain format.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool SendLoginInfo(string email, string username, string password)
        {
            var body = string.Format(
                "<div><strong>Login: </strong>{0}</div>" +
                "<div><strong>Password: </strong>{1}</div>",
                username, password);

            return Send(email, "Login Info", body);
        }
    }
}
