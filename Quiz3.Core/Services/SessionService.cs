﻿using Quiz3.Core.Models.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Persistence;

namespace Quiz3.Core.Services
{
    public class SessionService
    {
        private readonly UmbracoDatabase _db;

        public SessionService()
        {
            _db = ApplicationContext.Current.DatabaseContext.Database;
        }

        public Session Get(string title, int facilitorId)
        {
            return _db.FirstOrDefault<Session>("WHERE FacilitatorId = @0 AND Title = @1", facilitorId, title);
        }

        public Session Get(int id, int facilitorId)
        {
            return _db.FirstOrDefault<Session>("WHERE FacilitatorId = @0 AND Id = @1", facilitorId, id);
        }

        public Session Insert(string title, int facilitorId)
        {
            var record = Get(title, facilitorId);
            if (record != null)
            {
                return record;
            }

            var newRecord = new Session()
            {
                Title = title,
                FacilitatorId = facilitorId
            };

            _db.Insert(newRecord);
            return newRecord;
        }

        public IEnumerable<Session> GetAllForFacilitator(int facilitatorId)
        {
            return _db.Fetch<Session>("WHERE FacilitatorId = @0", facilitatorId);
        }
    }
}
