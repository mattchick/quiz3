﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence.Repositories;

namespace Quiz3.Core.Models
{
    public class LoginViewModel : BasePageViewModel
    {
        public LoginViewModel(IPublishedContent content)
            : base(content)
        {
            
        }
    }
}