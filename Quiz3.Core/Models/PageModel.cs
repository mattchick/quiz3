﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Quiz3.Core.Models
{
    public class PageModel
    {
        public PageModel(IPublishedContent content)
        {
            Name = content.Name;
            Url = content.Url;
        }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
