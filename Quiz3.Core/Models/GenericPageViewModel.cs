﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;
using Quiz3.Core.Models.Items;

namespace Quiz3.Core.Models
{
    public class GenericPageViewModel : BasePageViewModel
    {
        public GenericPageViewModel(IPublishedContent content)
            : base(content)
        {
            
        }
        
        public string Title { get; set; }
        public string Body { get; set; }
        public string TestTimeLimit { get; set; }
        public string TestInformationText { get; set; }
        public IPublishedContent TestInformationImage { get; set; }
        public string ActionButtonTitle { get; set; }
        public IPublishedContent ActionButtonLink { get; set; }

        public string FormIntroduction { get; set; }
        public string Form { get; set; }
        public bool HasForm { get; set; }
        public bool EnableTimer { get; set; }
        public bool TimeExpired { get; set; }
        
        public BorderColor BorderColor { get; set; }
        public string BodyClass { get; set; }

        public bool IsMemberSignedUpTheFormAlready { get; set; }

        public bool IsFacilitator { get; set; }
    }
}
