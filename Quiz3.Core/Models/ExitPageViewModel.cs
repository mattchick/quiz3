﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models.Base;
using Quiz3.Core.Models.Items;
using Umbraco.Core.Models;
using Umbraco.Forms.Core;

namespace Quiz3.Core.Models
{
    public class ExitPageViewModel : BasePageViewModel
    {
        public ExitPageViewModel(IPublishedContent content) : base(content)
        {
        }

        public string Title { get; set; }
        public string Body { get; set; }

        public BorderColor BorderColor { get; set; }
        public string BodyClass { get; set; }
        
    }
}
