﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;

namespace Quiz3.Core.Models
{
    public class PageViewModel : BasePageViewModel
    {
        public PageViewModel(IPublishedContent content) : base(content)
        {
        }
    }
}
