﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3.Core.Models.Forms
{
    public class SessionViewModel
    {
        [Required]
        [StringLength(200)]
        [DisplayName("Session Title")]
        public string Title { get; set; }
    }
}
