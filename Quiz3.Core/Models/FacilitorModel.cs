﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3.Core.Models
{
    public class FacilitorModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
