﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Forms.Core;
using Umbraco.Web.Models;

namespace Quiz3.Core.Models.Base
{
    public class BasePageViewModel : RenderModel
    {
        public BasePageViewModel(IPublishedContent content) : base(content)
        {
        }

        public BasePageViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
        {
        }

        public IPublishedContent Logo { get; set; }
        public bool DisableTimer { get; set; }
        public bool RenderScriptTimer { get; set; }
        public double Timer { get; set; }

        public bool IsExitPageDocType { get; set; }
        public List<Form> UmbracoForms { get; set; }

    }
}