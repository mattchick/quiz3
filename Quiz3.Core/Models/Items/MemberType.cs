﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3.Core.Models.Items
{
    public enum MemberType
    {
        Undefined, // default
        Member,
        Facilitator
    }
}
