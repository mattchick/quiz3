﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Quiz3.Core.Models.DataAccess
{
    [TableName("Session")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class Session
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        public int FacilitatorId { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
