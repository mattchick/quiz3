﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Quiz3.Core.Models.DataAccess
{
    [TableName("Form")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class Form
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Required]
        public int MemberId { get; set; }
        [Required]
        public Guid FormId { get; set; }
        [Required]
        public DateTime StartTime { get; set; }
    }
}
