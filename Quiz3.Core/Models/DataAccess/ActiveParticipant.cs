﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Quiz3.Core.Models.DataAccess
{
    [TableName("ActiveParticipant")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class ActiveParticipant
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }
        [Required]
        public int SessionId { get; set; }
        [Required]
        public int MemberId { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
    }
}
