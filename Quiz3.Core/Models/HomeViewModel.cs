﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;

namespace Quiz3.Core.Models
{
    public class HomeViewModel : BasePageViewModel
    {
        public HomeViewModel(IPublishedContent content)
            : base(content)
        {
            
        }
    }
}