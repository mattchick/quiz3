﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;
using Umbraco.Forms.Core;

namespace Quiz3.Core.Models
{
    public class FacilitorPageViewModel : BasePageViewModel
    {
        public FacilitorPageViewModel(IPublishedContent content) : base(content)
        {

        }
    }
}
