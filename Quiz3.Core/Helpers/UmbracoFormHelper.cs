﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Forms.Core;
using Umbraco.Forms.Data.Storage;

namespace Quiz3.Core.Helpers
{
    public class UmbracoFormHelper
    {
        /// <summary>
        /// Determine if member has answered the question already.
        /// </summary>
        public static bool IsMemberSignedUpAlready(Guid formId, int memberId)
        {
            FormStorage formStorage = new FormStorage();
            Form form;
            try
            {
                form = formStorage.GetForm(formId);
            }
            catch (Exception ex)
            {
                LogHelper.Error<UmbracoFormHelper>("Form is missing: " + formId, ex);
                return false;
            }
            
            RecordStorage recordStorage = new RecordStorage();
            var records = recordStorage.GetAllRecords(form, false);
            return records.Any(x =>
                x.MemberKey != null && // some forms has no member key
                int.Parse(x.MemberKey) == memberId);
        }
    }
}