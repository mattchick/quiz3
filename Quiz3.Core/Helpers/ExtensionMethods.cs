﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using umbraco.providers;
using Umbraco.Forms.Core;

namespace Quiz3.Core.Helpers
{
    public static class ExtensionMethods
    {
        public static string GetValue(this List<RecordField> field, string alias)
        {
            return field.FirstOrDefault(x => x.Alias == alias)?
                .Values.FirstOrDefault()?.ToString();
        }

        public static T ToEnum<T>(this string value, T defaultValue)
            where T : struct
        {
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            T result;
            return Enum.TryParse<T>(value, true, out result) ? result : defaultValue;
        }

        public static string GetUserName(this IIdentity identity)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return
                    HttpContext.Current.User.Identity.Name.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries)[1];
            }

            return string.Empty;
        }

        public static bool IsFacilitator(this IIdentity identity)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return
                    HttpContext.Current.User.Identity.Name.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries)[0] == "facilitator";
            }

            return false;
        }
    }
}