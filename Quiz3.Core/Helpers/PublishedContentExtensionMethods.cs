﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Quiz3.Core.Helpers
{
    public static class PublishedContentExtensionMethods
    {
        public static T GetPropertyAsEnum<T>(this IPublishedContent content, string alias, T defaultValue)
            where T : struct 
        {
            if (content.HasValue(alias))
            {
                var value = content.GetPropertyValue<string>(alias);
                return value.ToEnum(defaultValue);
            }
            
            return defaultValue;
        }
    }
}
