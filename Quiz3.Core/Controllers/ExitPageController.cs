﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Helpers;
using Quiz3.Core.Models;
using Quiz3.Core.Models.Items;
using Umbraco.Web;
using Umbraco.Forms.Data.Storage;

namespace Quiz3.Core.Controllers
{
    public class ExitPageController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<ExitPageViewModel>(CurrentPage);

            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.Body = CurrentPage.GetPropertyValue<string>("body");

            model.BorderColor = CurrentPage.GetPropertyAsEnum("borderColor", BorderColor.Yellow);
            model.BodyClass = CurrentPage.GetPropertyValue<string>("bodyClass");

            var fs = new FormStorage();
            model.UmbracoForms = fs.GetAllForms().ToList();

            return CurrentTemplate(model);
        }
    }
}
