﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Models;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize]
    public class HomeController : BasePageController
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel(CurrentPage);
            return CurrentTemplate(model);
        }
    }
}