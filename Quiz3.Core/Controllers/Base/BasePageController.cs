﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quiz3.Core.Models.Base;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Quiz3.Core.Controllers.Base
{
    public class BasePageController : RenderMvcController
    {
        public T GetModel<T>(IPublishedContent content)
            where T : BasePageViewModel
        {
            var model = Activator.CreateInstance(typeof (T), content) as BasePageViewModel;
            
            var root = content.AncestorOrSelf(1);

            model.Logo = root.GetPropertyValue<IPublishedContent>("logo");
            model.DisableTimer = root.GetPropertyValue<bool>("disableTimer");

            model.IsExitPageDocType = content.DocumentTypeAlias == "exitPage";
            
            return (T)model;
        }
    }
}