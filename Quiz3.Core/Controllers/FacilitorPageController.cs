﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Models;
using Umbraco.Forms.Data.Storage;
using Umbraco.Web;
using Umbraco.Web.Security;
using MemberType = Quiz3.Core.Models.Items.MemberType;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
    public class FacilitorPageController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<FacilitorPageViewModel>(CurrentPage);

            
            
            
            return CurrentTemplate(model);
        }
        
    }
}
