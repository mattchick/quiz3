﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.SessionState;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Helpers;
using Quiz3.Core.Models;
using Quiz3.Core.Models.Items;
using Quiz3.Core.Services;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Security;
using Quiz3.Core.Models.DataAccess;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize]
    public class GenericPageController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<GenericPageViewModel>(CurrentPage);
            var formService = new FormService();
            
            var currentMemberId = Members.GetCurrentMemberId();

            int timerInSeconds;
            var referencePage = CurrentPage.GetPropertyValue<IPublishedContent>("loadTimerFrom");
            model.EnableTimer = (CurrentPage.HasValue("timer") || referencePage != null) && !model.DisableTimer;

            var hasForm = CurrentPage.HasValue("selectForm");
            model.HasForm = hasForm;

            var formGuid = CurrentPage.GetPropertyValue<string>("selectForm");
            model.Form = formGuid;
            
            if (model.EnableTimer)
            {
                var dateTime = DateTime.Now;

                DateTime startTime;

                if (referencePage != null)
                {
                    // get form from reference page.
                    var referencePageForm = referencePage.GetPropertyValue<string>("selectForm");
                    var form = formService.Get(currentMemberId, new Guid(referencePageForm));
                    if (form == null)
                    {
                        formService.Insert(currentMemberId, new Guid(referencePageForm), dateTime);
                        startTime = dateTime;
                    }
                    else
                    {
                        startTime = form.StartTime;
                    }
                    
                    timerInSeconds = referencePage.GetPropertyValue<int>("timer");
                }

                else
                {
                    // get form from current page.
                    var form = formService.Get(currentMemberId, new Guid(model.Form));
                    if (form == null)
                    {
                        formService.Insert(currentMemberId, new Guid(model.Form), dateTime);
                        startTime = dateTime;
                    }
                    else
                    {
                        startTime = form.StartTime;
                    }
                    timerInSeconds = CurrentPage.GetPropertyValue<int>("timer");
                }
                
                var dateDiff = dateTime - startTime;
                var lapseTimeInMilliseconds = dateDiff.TotalMilliseconds;

                var timerInMilliseconds = timerInSeconds * 1000;
                var timeLeftInMs = timerInMilliseconds - lapseTimeInMilliseconds;
                model.Timer = timeLeftInMs;
                model.TimeExpired = model.EnableTimer && timeLeftInMs < 0;

            }

            model.Title = CurrentPage.GetPropertyValue<string>("title");
            model.Body = CurrentPage.GetPropertyValue<string>("body");
            model.TestTimeLimit = CurrentPage.GetPropertyValue<string>("testTimeLimit");
            model.TestInformationText = CurrentPage.GetPropertyValue<string>("testInformationText");
            model.TestInformationImage = CurrentPage.GetPropertyValue<IPublishedContent>("testInformationImage");
            model.ActionButtonTitle = CurrentPage.GetPropertyValue<string>("actionButtonTitle");
            model.ActionButtonLink = CurrentPage.GetPropertyValue<IPublishedContent>("actionButtonLink");
            model.FormIntroduction = CurrentPage.GetPropertyValue<string>("formIntroduction");
            
            model.BorderColor = CurrentPage.GetPropertyAsEnum("borderColor", BorderColor.Yellow);
            model.BodyClass = CurrentPage.GetPropertyValue<string>("bodyClass");

            if (hasForm)
            {
                model.IsMemberSignedUpTheFormAlready = UmbracoFormHelper.IsMemberSignedUpAlready(new Guid(formGuid), currentMemberId);
            }

            model.RenderScriptTimer = model.EnableTimer && !model.TimeExpired && !model.IsMemberSignedUpTheFormAlready;

            model.IsFacilitator = Members.IsMemberAuthorized(allowTypes: new List<string> {"Facilitator"});

            return CurrentTemplate(model);
        }
    }
}
