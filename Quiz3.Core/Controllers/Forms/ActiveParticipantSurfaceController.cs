﻿using Quiz3.Core.Models.Forms;
using Quiz3.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Quiz3.Core.Helpers;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using Umbraco.Web.Security;
using Umbraco.Web;
using Umbraco.Core.Models;
using MemberType = Quiz3.Core.Models.Items.MemberType;

namespace Quiz3.Core.Controllers.Forms
{
    [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
    public class ActiveParticipantSurfaceController : SurfaceController
    {
        private readonly IMemberService _memberService;
        private readonly ActiveParticipantService _activeParticipantService;
        
        public ActiveParticipantSurfaceController()
        {
            _memberService = Services.MemberService;
            _activeParticipantService = new ActiveParticipantService();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ParticipantViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            // check session ownership
            var sessionService = new SessionService();
            var isValid = sessionService.Get(model.SessionId,
                _memberService.GetByUsername(User.Identity.Name).Id) != null;

            if (!isValid)
            {
                ModelState.AddModelError("", "Invalid Session");
                return CurrentUmbracoPage();
            }

            // Register member
            var registerModel = Members.CreateRegistrationModel("member");

            var username = model.FirstName.Trim() + ' ' + model.Lastname.Trim();
            registerModel.Name = username;
            registerModel.Email = model.EmailAddress;
            registerModel.Username = username;
            registerModel.Password = Membership.GeneratePassword(6, 0);
            registerModel.UsernameIsEmail = false;

            MembershipCreateStatus status;
            var newMember = Members.RegisterMember(registerModel, out status, false);
            
            switch (status)
            {
                case MembershipCreateStatus.Success:
                    _activeParticipantService.Insert(_memberService.GetByUsername(newMember.UserName).Id,
                        model.SessionId);
                    return Redirect("/facilitor/view-session/session/manage-participants?id=" + model.SessionId);
                case MembershipCreateStatus.DuplicateEmail:
                    ModelState.AddModelError("", "Email already taken.");
                    return CurrentUmbracoPage();
                case MembershipCreateStatus.DuplicateUserName:
                    ModelState.AddModelError("", "Username already taken");
                    return CurrentUmbracoPage();
                default:
                    ModelState.AddModelError("", "Error.");
                    return CurrentUmbracoPage();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int memberId, int sessionId)
        {
            // delete from backoffice
            var umbmember = _memberService.GetById(memberId);
            if (umbmember != null)
            {
                _memberService.Delete(umbmember);
            }

            // delete from database
            _activeParticipantService.Delete(memberId, sessionId);

            TempData["message"] = "Member Deleted";

            return RedirectToCurrentUmbracoPage("?id=" + sessionId);

        }
    }
}
