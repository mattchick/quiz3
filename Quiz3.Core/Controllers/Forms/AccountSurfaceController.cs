﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using Quiz3.Core.Models.Forms;
using Umbraco.Web.Mvc;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Quiz3.Core.Helpers;
using Quiz3.Core.Services;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Security;
using Umbraco.Web.Security.Providers;
using MemberType = Quiz3.Core.Models.Items.MemberType;

namespace Quiz3.Core.Controllers.Forms
{
    [Utilities.MemberAuthorize]
    public class AccountSurfaceController : SurfaceController
    {
        private readonly IMemberService _memberService;

        public AccountSurfaceController()
        {
            _memberService = Services.MemberService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }
            
            var success = Members.Login(model.Username, model.Password);
            
            if (success)
            {
                var memberService = ApplicationContext.Services.MemberService;
                var isFacilitator = memberService.GetByUsername(model.Username).ContentTypeAlias ==
                                    Models.Items.MemberType.Facilitator.ToString();

                if (isFacilitator)
                {
                    return RedirectToUmbracoPage(Umbraco.TypedContentSingleAtXPath("//facilitorPage"));
                }

                if (CurrentPage.HasValue("redirectUrl"))
                {
                    return RedirectToUmbracoPage(CurrentPage.GetPropertyValue<IPublishedContent>("redirectUrl"));
                }

                return RedirectToCurrentUmbracoPage();
            }

            ModelState.AddModelError("", "Invalid login attempt.");
            return CurrentUmbracoPage();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            Members.Logout();
            return RedirectToUmbracoPage(Umbraco.TypedContentSingleAtXPath("//home"));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            var registerModel = Members.CreateRegistrationModel("member");
            registerModel.Email = model.Email;
            registerModel.Password = model.Password;
            registerModel.Name = model.Name;
            registerModel.Username = model.Username;

            MembershipCreateStatus status;
            Members.RegisterMember(registerModel, out status);

            switch (status)
            {
                case MembershipCreateStatus.Success:
                    return RedirectToUmbracoPage(Umbraco.TypedContentSingleAtXPath("//home"));
                case MembershipCreateStatus.DuplicateEmail:
                    ModelState.AddModelError("", "Email already taken.");
                    return CurrentUmbracoPage();
                case MembershipCreateStatus.DuplicateUserName:
                    ModelState.AddModelError("", "Username already taken");
                    return CurrentUmbracoPage();
                default:
                    ModelState.AddModelError("", "Error.");
                    return CurrentUmbracoPage();
            }
        }

        [HttpPost]
        [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
        public ActionResult ResendLogin(int memberId, int sessionId)
        {
            var member = _memberService.GetById(memberId);
            if (member != null)
            {
                EmailService.SendLoginInfo(member.Email, member.Username, member.RawPasswordValue);
            }
            
            TempData["message"] = "Sent!";
            return RedirectToCurrentUmbracoPage("?id=" + sessionId);
        }
    }
}