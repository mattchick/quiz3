﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Quiz3.Core.Helpers;
using Quiz3.Core.Models.Forms;
using Quiz3.Core.Models.Items;
using Quiz3.Core.Services;
using Umbraco.Web.Mvc;

namespace Quiz3.Core.Controllers.Forms
{
    [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
    public class SessionSurfaceController : SurfaceController
    {

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(SessionViewModel model)
        {
            var ms = ApplicationContext.Services.MemberService;
            var currentMember = ms.GetByUsername(User.Identity.Name);

            var sessionService = new SessionService();
            var session = sessionService.Insert(model.Title, currentMember.Id);

            return Redirect("/facilitor/view-session/session?id=" + session.Id);
        }
    }
}
