﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Models;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize]
    public class LoginController : BasePageController
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            var model = new LoginViewModel(CurrentPage);

            return CurrentTemplate(model);
        }
    }
}