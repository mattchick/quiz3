﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Quiz3.Core.Services;
using Umbraco.Core.Logging;
using Umbraco.Web.WebApi;

namespace Quiz3.Core.Controllers
{
    public class HelperApiController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public void Send()
        {
            //string from = "postmaster@mail.qualityeveryday.com";
            EmailService.Send("renanteabril@gmail.com", "Testing SMTP subject", "Testing SMTP body");
        }
    }
}
