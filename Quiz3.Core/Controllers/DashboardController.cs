﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Models;
using Quiz3.Core.Models.Items;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
    public class DashboardController : BasePageController
    {
        public ActionResult Index()
        {
            var model = new DashboardViewModel(CurrentPage);

            return CurrentTemplate(model);
        }
    }
}