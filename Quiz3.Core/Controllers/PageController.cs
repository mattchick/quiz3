﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Quiz3.Core.Controllers.Base;
using Quiz3.Core.Models;
using Quiz3.Core.Models.Items;

namespace Quiz3.Core.Controllers
{
    [Utilities.MemberAuthorize(AllowType = MemberType.Facilitator)]
    public class PageController : BasePageController
    {
        public ActionResult Index()
        {
            var model = GetModel<PageViewModel>(CurrentPage);

            return CurrentTemplate(model);
        }
    }
}
