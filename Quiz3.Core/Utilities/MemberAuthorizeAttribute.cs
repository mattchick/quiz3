﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Quiz3.Core.Helpers;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Security;
using Umbraco.Web.Security.Providers;
using Quiz3.Core.Models.Items;

namespace Quiz3.Core.Utilities
{
    /// <summary>
    /// Alternative to Umbraco AuthorizeAttribute redirect to login not working at web.config.
    /// </summary>
    public class MemberAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly IMemberService _memberService;
        private readonly MembershipHelper _membershipHelper;

        public MemberAuthorizeAttribute()
        {
            _memberService = ApplicationContext.Current.Services.MemberService;
            _membershipHelper = new MembershipHelper(UmbracoContext.Current);
        }
        
        public MemberType AllowType { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isLoggedIn = httpContext.User.Identity.IsAuthenticated;

            if (!isLoggedIn)
            {
                return false;
            }

            if (AllowType != MemberType.Undefined)
            {
                var member = _memberService.GetByUsername(httpContext.User.Identity.Name);
                return member.ContentTypeAlias == AllowType.ToString();
            }

            return true;
        }
    }
}